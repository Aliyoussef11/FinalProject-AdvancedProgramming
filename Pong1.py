import turtle
import os 

wn = turtle.Screen()
wn.title("Pong")
wn.bgpic("Background.png")
wn.setup(width=800, height=600)
wn.tracer(0)

# Score

score_a = 0
score_b = 0

# Paddle A
paddle_a = turtle.Turtle()
paddle_a.speed(0)
paddle_a.shape("square")
paddle_a.color("Cyan")
paddle_a.shapesize(stretch_wid=5, stretch_len=1)
paddle_a.penup()
paddle_a.goto(-350, 0)

# Paddle B

paddle_b = turtle.Turtle()
paddle_b.speed(0)
paddle_b.shape("square")
paddle_b.color("Cyan")
paddle_b.shapesize(stretch_wid=5, stretch_len=1)
paddle_b.penup()
paddle_b.goto(350, 0)

# Ball

ball = turtle.Turtle()
ball.speed(0)
ball.shape("square")
ball.color("yellow")
ball.penup()
ball.goto(0, 0)
ball.dx = 2
ball.dy = -2


# pen

pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write("Player A: 0   Player B: 0", align="center", font=("courier", 24, "normal"))



#function Up_Down for Paddle A

def paddle_a_up():
    y = paddle_a.ycor()
    y += 20
    paddle_a.sety(y)


def paddle_a_down():
    y = paddle_a.ycor()
    y -= 20
    paddle_a.sety(y)


#function Up_Down for Paddle B

def paddle_b_up():
    y = paddle_b.ycor()
    y += 20
    paddle_b.sety(y)

def paddle_b_down():
    y = paddle_b.ycor()
    y -= 20
    paddle_b.sety(y)

# keyboard binding

wn.listen()
wn.onkey(paddle_a_up, "w")
wn.onkey(paddle_a_down, "s")
wn.onkey(paddle_b_up, "Up")
wn.onkey(paddle_b_down, "Down")


#game loop

while True:
      wn.update()

      # Move the ball
      ball.setx(ball.xcor() + ball.dx)
      ball.sety(ball.ycor() + ball.dy)

      # Border check
      if ball.ycor() > 290:
          ball.sety(290)
          ball.dy *= -1
          os.system("aplay bounce.wav&")
     
      if ball.ycor() < -290:
          ball.sety(-290)
          ball.dy *= -1
          os.system("aplay bounce.wav&")


      if ball.xcor() > 390:
          ball.goto(0, 0)
          ball.dx *= -1
          score_a += 1
          pen.clear()
          pen.write("Player A: {}   Player B: {}".format(score_a, score_b), align="center", font=("courier", 24, "normal"))

      if ball.xcor() < -390:
          ball.goto(0, 0)
          ball.dx *= -1
          score_b += 1
          pen.clear()
          pen.write("Player A: {}   Player B: {}".format(score_a, score_b), align="center", font=("courier", 24, "normal"))


      # paddle and ball collisions
      if (ball.xcor() > 340 and ball.xcor() < 350) and (ball.ycor() < paddle_b.ycor() + 40 and ball.ycor() > paddle_b.ycor() -50):
          ball.setx(340)
          ball.dx *= -1
          os.system("aplay bounce.wav&")

 
      if (ball.xcor() < -340 and ball.xcor() < -350) and (ball.ycor() < paddle_a.ycor() + 40 and ball.ycor() > paddle_a.ycor() -50):
          ball.setx(-340)
          ball.dx *= -1
          os.system("aplay bounce.wav&")

      
#add buttom and top borders




      if paddle_a.ycor() < -230:
         paddle_a.goto(-350,-230)
         
      if paddle_b.ycor() < -230:
         paddle_b.goto(350,-230)
         
      if paddle_a.ycor() > 230:
         paddle_a.goto(-350,230)
         
      if paddle_b.ycor() > 230:
         paddle_b.goto(350,230)
         
